import os
import argparse # to parse command line options
import re
import shutil

def HypoTesterSteeringScriptDefinition(RooWorkspaceInputFile, pointsToScan, nToys, rucioScope,
    workspaceName = "combined", ModelConfig = "ModelConfig", dataset = "obsData",jobNumber = "0"):

    scope = re.match("^.*(?=(\:))", rucioScope).group()


    steeringScriptList = [
    "#!/bin/sh", 
    "# setup ATLAS environment and rucio", 
    "echo \"setupATLAS\"", 
    "source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh ", 
    "echo \"localSetupRucioClients\"", 
    "source ${ATLAS_LOCAL_ROOT_BASE}/utilities/oldAliasSetup.sh rucio", 

    "RooWorkspaceInputFile=\"%s\"" %RooWorkspaceInputFile, 
    "workspaceName=\"%s\"" %workspaceName, 
    "ModelConfig=\"%s\"" %ModelConfig, 
    "dataset=\"%s\"" %dataset, 
    "jobNumber=\"%s\"" %jobNumber, 
    "pointsToScan=\"%s\"" %pointsToScan, 
    "nToys=\"%s\"" %nToys, 

    "rucioScope=\"%s\"" %rucioScope, 
    "scope=\"%s\"" %scope, 

    "workDir=$PWD",
    "echo \"the current woring dir is: $workDir\"",
    "echo \"current files in the working directory are :\"",
    "ls -l",
    "du -hs *",

    "md5sum $RooWorkspaceInputFile",
    "md5sum  $workDir/$RooWorkspaceInputFile",

    "##setup the appropriate root version", 
    "source /workdir/root_v6-08-00_install/bin/thisroot.sh", 

    "# check if we need to do the ToyGeneration step or the stat combining step:", 

    "if grep -q \"stat\" \"input.json\"", 
    "then", 
    "    echo \"do combination\"", 
    "    #define name for combined result",
    "    timestamp=`date +\"%Y%m%d_%H%M%S\"` ",
    "    combinationOutputFileName=\"combinedMCToyResults_$timestamp.root\" ",
    "    echo $combinationOutputFileName", 
    "    #download intermediary files via rucio", 
    "    rucio download $rucioScope", 
    "    #downloaded files are in this folder: (i.e. everything after the \":\" in the rucio scope)", 
    "    downloadedFolder=\"$(echo \"$rucioScope\" | grep -oP '(?<=:).*')\"", 
    "    # run the combination ", 
    "    /./workdir/StandardHypoTestInv/StandardHypoTestInv -r $combinationOutputFileName $workDir/$downloadedFolder/*.root", 
    "    #upload the combined file", 
    "    echo \"rucio upload --rse CERN-PROD_SCRATCHDISK  --scope $scope $rucioScope $workDir/$combinationOutputFileName\"", 
    "    rucio upload --rse CERN-PROD_SCRATCHDISK  --scope $scope $rucioScope $workDir/$combinationOutputFileName ", 
    "    # write loss function file, important so that iDDS keeps working", 
    "    echo \"{ \\\"status\\\":0, \\\"loss\\\": 0.0  }\" > output.json", 
    "else", 
    "    echo \"make MC Toys\"", 
    "    # define output file name in case we are doing mcToy calculation", 
    "    timestampAndRandomTag=`date +\"%Y%m%d_%H%M%S_\"$RANDOM`", 
    "    echo $timestampAndRandomTag", 
    "    mcToyOutputFileName=$timestampAndRandomTag\"_mcToyResult.root\" ", 
    "    echo $mcToyOutputFileName", 
    "    mcToySkimFileName=$timestampAndRandomTag\"_mcToyResult.skim.root\" ", 
    "    seed=`od -vAn -N4 -tu4 < /dev/urandom` # seed for the MC Toy calculation, set explicitly such that it is assuredly different for each iteration", 
    "    echo \"seed: $seed\"", 
    "    #run MCToy calculations", 
    "    echo \"run MCToy calculations with seed\"", 
    "    #  StandardHypoTestInv WORKSPACE-FILE.root \\", 
    "    #  -w workspace -m modelConfig -d dataset \\", 
    "    #  -a -p jobNum -n nPointsToScan -N points/job -P pointsToScan \\", 
    "    #  -t nToys[,nToysBkg] -M invMass -0 scanMin -1 scanMax -2 poimax -c confidenceLevel \\", 
    "    #  -W nProofWorkers -C -O OPT-LEVEL -H -r OUTPUT-RESULT-FILE", 
    "    /./workdir/StandardHypoTestInv/StandardHypoTestInv $workDir/$RooWorkspaceInputFile -w $workspaceName -m $ModelConfig -d $dataset -p $jobNumber -P $pointsToScan -a -t $nToys -r  $workDir/$mcToyOutputFileName -S $seed", 
    "    # skim the outputs. Skimmed files contain only the information relevant for the statistical combination", 
    "    # RAM usage is much lower that way", 
    "    rootcp  $workDir/$mcToyOutputFileName:result_mu $workDir/$mcToySkimFileName", 
    "    # write loss function file, important so that iDDS keeps workig", 
    "    echo \"{ \\\"status\\\":0, \\\"loss\\\": 0.0  }\" > output.json", 
    "    #upload MCToy output file explicitly to rucio", 
    "    echo \"rucio upload --rse CERN-PROD_SCRATCHDISK  --scope $scope $rucioScope $workDir/$mcToySkimFileName\"", 
    "    rucio upload --rse CERN-PROD_SCRATCHDISK  --scope $scope $rucioScope $workDir/$mcToySkimFileName ", 
    "    echo \"rucio list-files $rucioScope\"", 
    "    rucio list-files $rucioScope", 
    "fi"]

    return steeringScriptList

def makeSearchSpaceFile(outputFileName = "search_space.json"):

    searchSpaceDefinition=[
    "{",
    "    \"batch_size\": {",
    "      \"method\": \"uniformint\",",
    "      \"dimension\": {",
    "        \"low\": 0,",
    "        \"high\": 1",
    "      }",
    "    }",
    "  }"]

    with open(outputFileName, "w") as writeJsonFile:
        for line in searchSpaceDefinition: writeJsonFile.write(line+"\n" )

    return None


def writeSteeringScript( steeringScriptDef, steeringScriptName = "iDDsHypoTester_SteeringScript.sh"):

    with open(steeringScriptName, "w") as writeSteeringScript:
        for line in steeringScriptDef: writeSteeringScript.write(line+"\n" )

    os.chmod(steeringScriptName, 0o755) # make sure submit script is executable

    return steeringScriptName

def prepInputFile(inputFile):
    # idds does not seem to copy root files (anymore). 
    # But it still copies .sh files. So let's just make a copy of the input file with .sh ending
    inputFilePath = os.path.abspath(inputFile)

    newFilePath = inputFilePath +".sh" 

    shutil.copy(inputFilePath,newFilePath)

    newFileName = os.path.basename(newFilePath)

    return newFileName

def initilizeArgParser( parser = argparse.ArgumentParser(
    description="Script to calculate Monte Carlo (MC) toy based limits on RooWorkspaces on the grid, via iDDS.\
    Toy based limit calculation is done via the ROOT RooStats.StandardHypoTestInv.\
    iDDs is the intelligent data delivery service\
    The purpose of the script is to allow performant parallel calculations of MC Toy limits on the grid.\
    Each parallel calculation yields an independent RooStats.HypoTestInverterResult objects.\
    The script also processes them into a combined RooStats.HypoTestInverterResult automatically.\
    The independent and combined HypoTestInverterResult files can be downloaded from the user specific rucio location.\
    \
    Execute the following commands to load the required environment: \
        \
        export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase  \
        setupATLAS    \
        lsetup panda  \
        localSetupRucioClients; voms-proxy-init -voms atlas") ):

    parser.add_argument("--inputFile", type=str, required=True,
        help="Mandatory option. \
        Name of the .root file that contains the RooWorkspace defining the model and data to be statistically evaluated via RooStats.StandardHypoTestInv.\
        e.g. '--inputFile analysisWorkspaceFile.root" )

    parser.add_argument("--pointsToScan", type=str, required=True,
        help="Mandatory option. \
        Defines a list of values for the models' parameter of interest\
        can be a list of points or point-steps for the models' parameter of interest (PoI) to scan over,\
        e.g. '--pointsToScan 0.1,0.2,0.5,1.0' let's the PoI take the values 0.1, 0.2, 0.5, 1.0,\
        e.g. '--pointsToScan 0.00,1.35/0.02' let's the PoI take the values between 0 and 1.35 in steps of 0.02\
        e.g. '--pointsToScan 2/0.1,/0.5' means every 0.1 up to 2, then every 0.5 up to the maximum of the PoI.")

    parser.add_argument("--nToys", type=str, required=True,
        help="Mandatory option.\
        NS[,NB] number of toys per point for SB and B per calculations\
        e.g. '--nToys 100' or '--nToys 100,100'      ")

    parser.add_argument("--rucioScope", type=str, required=True,
        help="Mandatory option.\
        Rucio location for storing the intermediary, and final, combined HypoTestInverterResult \
        e.g. '--rucioScope  user.MaxMustermann:user.MaxMustermann.IDDSLimitsForAnalysis'     ")

    parser.add_argument("--nParallelComputations", type=int, required=True,
        help="Number of times we perform the above calculations above\
        Launches <nParallelComputations>+1 jobs, where the last one performs the combination of the <nParallelComputations> intermediary results.\
        e.g. '--nParallelComputations 10' ")

    parser.add_argument("--workspaceName", type=str, default="combined" , 
        help="Name of the RooWorkspace in the input file we want to use for the limits\
        e.g. '--workspaceName combined'")

    parser.add_argument("--ModelConfig", type=str, default="ModelConfig" , 
        help="Name of the ModelConfig in the RooWorkspace for the limits\
        e.g. '--ModelConfig ModelConfig'")

    parser.add_argument("--dataset", type=str, default="obsData" , 
        help="Name of the dataset in the RooWorkspace that we want to use for the observed limits\
        e.g. '--dataset obsData'")

    args = parser.parse_args()

    return args

if __name__ == '__main__':

    # parse command line arguments
    args = initilizeArgParser()

    workspaceFile = prepInputFile(args.inputFile)

    steeringScriptDef = HypoTesterSteeringScriptDefinition(workspaceFile, args.pointsToScan, args.nToys, args.rucioScope,
        workspaceName = args.workspaceName, ModelConfig = args.ModelConfig, dataset = args.dataset)

    steeringScriptName = writeSteeringScript( steeringScriptDef)
    makeSearchSpaceFile()    

    # (?<=:).*   --  everything after the first colon ":"
    rucioOutDSParam = re.search("(?<=:).*",args.rucioScope).group() # get everything after the colon in the rucio dataset name
    rucioUserName = re.search("(?<=user\.).*(?=(\:))", args.rucioScope).group()

    iDDsCommand = "phpo  --outDS %s --searchSpaceFile='search_space.json' --evaluationMetrics='metrics.tgz' --nParallelEvaluation=2 --nPointsPerIteration=%i --evaluationExec='%s ' --evaluationContainer='docker://christiantmweber/standardhypotestinverter_for_idds:v21_11_28'  --steeringExec='run --rm -v \"$(pwd)\":/data -v /cvmfs:/cvmfs -e X509_USER_PROXY=/data/x509up -e RUCIO_ACCOUNT=%s wguanicedew/idds_hpo_toymc:latest python /opt/hyperparameteropt_toymc.py --max_points=%%MAX_POINTS --num_points=%%NUM_POINTS --input=/data/%%IN --output=/data/%%OUT' --maxPoints=%i    " %( rucioOutDSParam, args.nParallelComputations, steeringScriptName, rucioUserName, args.nParallelComputations+1)

    os.system( iDDsCommand) # submit the command, and with it the computations on iDDs

    #import pdb; pdb.set_trace()