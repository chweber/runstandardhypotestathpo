#!/bin/sh

# setup ATLAS environment and rucio
echo "setupATLAS"
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh 
echo "localSetupRucioClients"
source ${ATLAS_LOCAL_ROOT_BASE}/utilities/oldAliasSetup.sh rucio

##setup root


#rucioScope="user.chweber:user.chweber.testUpload01"
#rucioScope="user.$RUCIO_ACCOUNT:user.$RUCIO_ACCOUNT.testUpload01"

RUCIO_ACCOUNT_NAME=chweber

rucioScope="user.$RUCIO_ACCOUNT_NAME:user.$RUCIO_ACCOUNT_NAME.testUpload02"

##setup the appropriate root version
source /workdir/root_v6-08-00_install/bin/thisroot.sh


workDir=$PWD
echo "the current woring dir is: $workDir"
echo "current files in wokring directory:"
ls

"test" > test05.txt

cat input.json

# check if we need to do the ToyGeneration step or the stat combining step:

if grep -q "stat" "input.json"
then
	echo "do combination"
	doCombination=true

	#download intermediary files via rucio

	rucio download $rucioScope

	#downloaded files are in this folder: (i.e. everything after the ":" in the rucio scope)
	downloadedFolder="$(echo "$rucioScope" | grep -oP '(?<=:).*')"


	# run the combination 
	/./workdir/StandardHypoTestInv/StandardHypoTestInv -r CombinedMCToyResults.root $workDir/$downloadedFolder/*.root

	#upload the combined file
	echo "rucio upload --rse CERN-PROD_SCRATCHDISK  --scope user.chweber $rucioScope $workDir/CombinedMCToyResults.root"
	rucio upload --rse CERN-PROD_SCRATCHDISK  --scope user.chweber $rucioScope $workDir/CombinedMCToyResults.root 

	# write loss function file, important so that iDDS keeps workig
	echo "{ \"status\":0, \"loss\": 0.0  }" > output.json



else
	echo "make MC Toys"
	doCombination=false


	# define output file name in case we are doing mcToy calculation

	timestampAndRandomTag=`date +"%Y%m%d_%H%M%S_"$RANDOM`
	echo $timestampAndRandomTag

	mcToyOutputFileName=$timestampAndRandomTag"_mcToyResult.root" 
	echo $mcToyOutputFileName

	mcToySkimFileName=$timestampAndRandomTag"_mcToyResult.skim.root" 

	seed=`od -vAn -N4 -tu4 < /dev/urandom` # seed for the MC Toy calculation, set explicitly such that it is assuredly different for each iteration

	echo "seed: $seed"

	#run MCToy calculations
	echo "run MCToy calculations with seed"
	/./workdir/StandardHypoTestInv/StandardHypoTestInv /workdir/StandardHypoTestInv/exampleWorkspace.root -w combined -m ModelConfig -d obsData -p 0 -P 0,1,2 -a -t 50,50 -r  $workDir/$mcToyOutputFileName -S $seed

	# skim the outputs. Skimmed files contain only the information relevant for the statistical combination
	# RAM usage is much lower that way
	rootcp  $workDir/$mcToyOutputFileName:result_mu $workDir/$mcToySkimFileName


	# write loss function file, important so that iDDS keeps workig
	echo "{ \"status\":0, \"loss\": 0.0  }" > output.json

	# add the ouput file to the metrics tar file, that way the output file get's uploaded to rucio
	#tar -czvf metrics.tgz $mcToyOutputFileName

	echo "current files in wokring directory, after toy calculation:"
	ls

	#upload MCToy output file explicitly to rucio

	#echo "rucio upload --rse CERN-PROD_SCRATCHDISK $rucioScope $workDir/$mcToyOutputFileName --scope user.chweber"
	#rucio upload --rse CERN-PROD_SCRATCHDISK $rucioScope $workDir/$mcToyOutputFileName --scope user.chweber


	#echo "rucio list-files $rucioScope"
	#rucio list-files $rucioScope

	

	#echo "rucio upload --rse CERN-PROD_SCRATCHDISK  --scope user.chweber $rucioScope $workDir/test05.txt"
	#rucio upload --rse CERN-PROD_SCRATCHDISK --scope user.chweber $rucioScope $workDir/test05.txt 

	echo "rucio upload --rse CERN-PROD_SCRATCHDISK  --scope user.chweber $rucioScope $workDir/$mcToySkimFileName"
	rucio upload --rse CERN-PROD_SCRATCHDISK  --scope user.chweber $rucioScope $workDir/$mcToySkimFileName 


	echo "rucio list-files $rucioScope"
	rucio list-files $rucioScope


fi
