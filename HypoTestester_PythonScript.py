#!/usr/bin/env python

print("Starting python script")

import os
import datetime     # so that we know the time
import random
import string


if __name__ == '__main__':

    currentWorkingDir = os.path.abspath(os.getcwd())

    rucioScope = "user.chweber:user.chweber.testUpload01"



    print("Current working dir")
    print(currentWorkingDir)

    print("Working dir contents")
    print(os.listdir(currentWorkingDir) )


    doCombining = False

    print("Check 'input.json' file: ")

    with open('input.json', 'r') as readFile:
        for line in readFile: 
            print(line)
            if "stat" in line: doCombining = True

    # Further file processing goes here

    print("Load root version")
    os.system("source /workdir/root_v6-08-00_install/bin/thisroot.sh")

    load_ROOT_command = "source /workdir/root_v6-08-00_install/bin/thisroot.sh"


    now = datetime.datetime.now()
    date = now.strftime("%Y%m%d_%H%M%S")
    nrOfRandonCharacters = 10
    randomTag = "_"+''.join(random.choice(string.ascii_uppercase + string.ascii_lowercase + string.digits) for _ in range(nrOfRandonCharacters))


    rootOutputFileName = "hypoTesterOut_" + date + randomTag +".root"


    run_MCToy_command = (" /./workdir/StandardHypoTestInv/StandardHypoTestInv "
        "/workdir/StandardHypoTestInv/exampleWorkspace.root -w combined -m ModelConfig -d obsData -p 0 -P 0,1,2 -a -t 50,50 -r  "
        "$workDir/%s" %rootOutputFileName)

    if doCombining: 
        print("Do stat combining step")

        print(os.listdir(currentWorkingDir) )

        combiningCommand = "/./workdir/StandardHypoTestInv/StandardHypoTestInv -r ResultsPostProcessed.root $workDir/hypoTesterOut*.root"


        os.system( ";".join([load_ROOT_command, combiningCommand]))


        print(os.listdir(currentWorkingDir) )

        print("Upload test file to rucio")
        os.system("rucio upload --rse CERN-PROD_SCRATCHDISK %s $workDir/%s" %(rucioScope , "hypoTesterOut1.root"))




    else: 
        print("run MC Toy calculation")


        os.system( ";".join([load_ROOT_command, run_MCToy_command]))

        print("Write loss functions")
        os.system('echo "{ \"status\":0, \"loss\": 0.0  }" > output.json')

        print("Make 'metrics.tgz' tar file")
        os.system("tar -czvf metrics.tgz hypoTesterOut.root")

        print("Check rucio")


        #os.system("localSetupRucioClients")
        os.system("rucio list-dids user.chweber:user.chweber.test_idds_hpo%")

        print("List Working dir contents again prior to uplodaing to rucio")
        print(os.listdir(currentWorkingDir) )

        print("Upload root output file to rucio")
        os.system("rucio upload --rse CERN-PROD_SCRATCHDISK %s $workDir/%s" %(rucioScope , rootOutputFileName))



    #import pdb; pdb.set_trace() # import the debugger and instruct it to stop here