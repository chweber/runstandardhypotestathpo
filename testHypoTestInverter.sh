#!/bin/sh

echo $PWD

ls

workDir=$PWD

source /workdir/root_v6-08-00_install/bin/thisroot.sh

/./workdir/StandardHypoTestInv/StandardHypoTestInv /workdir/StandardHypoTestInv/exampleWorkspace.root -w combined -m ModelConfig -d obsData -p 0 -P 0,1,2 -a -t 50,50 -r  $workDir/hypoTesterOut.root


ls

echo "{ \"status\":0, \"loss\": 0.0  }" > output.json

tar -czvf metrics.tgz hypoTesterOut.root
